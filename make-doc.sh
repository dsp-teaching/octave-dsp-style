#!/usr/bin/bash

#
# Run this script to recreate README.pdf from the README.md. You
# need to have pandoc (and, I guess, latex) installed.
#

pandoc -f markdown+lists_without_preceding_blankline \
       -o README.pdf                                 \
       README.md
