# What is this?

This directory contains `linear_interp.m` which is an example of virtual
handler that interpolates its input by a factor of 2.  It is an example
that outputs more than one sample at every call.

You can run the example by setting this directory to the octave current
directory and calling `run_linear_interp`.
