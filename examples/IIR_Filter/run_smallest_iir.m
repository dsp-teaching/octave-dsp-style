addpath ../../src

impulse = [1024, zeros(1, 10)];  % 1024 \delta(n)
result = run_dsp_simulation(impulse,       ...  % Read data from vector
			    [],            ...  % No output file
			    @smallest_iir, ...  % virtual handler
                            0.7)                % INIT_DATA=filter coeff
