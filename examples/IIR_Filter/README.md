# What is this?

This directory contains `smallest_iir.m` which is an example of virtual
handler that filters its input with a one pole IIR filter.  The filter
coefficient is set at boot time via the INIT_DATA parameter.

You can run the example by setting this directory to the octave current
directory and calling `run_smallest_iir`.
